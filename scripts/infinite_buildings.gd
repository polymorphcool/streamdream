#tool

extends Spatial

export(bool) var generate = false
export(bool) var purge_all = false

export(Vector3) var start_min = Vector3( -4, -1, -500 )
export(Vector3) var start_max = Vector3( 4, 1, -200 )
export(Vector3) var end_min = Vector3( -20, -5, -510 )
export(Vector3) var end_max = Vector3( 20, 5, 10 )

export(Vector3) var camera_speed = Vector3(0,0,5)
export(Vector3) var cube_size_variation = Vector3(0,0,0)
export(Vector3) var cube_rot_variation = Vector3(0,0,0)
export(Vector3) var tree_size_variation = Vector3(0,0,0)
export(Vector3) var tree_rot_variation = Vector3(0,0,0)
export(float,0,1) var cube_tree_ratio = 0.5
export(int,0,300) var max_instances = 5

export(Vector3) var linear_speed = Vector3()
export(float,0,1) var linear_decay = 0
export(bool) var randomise_linear = false

export(Vector3) var radial_speed = Vector3()
export(float,0,1) var radial_decay = 0
export(bool) var randomise_radial = false

export(String) var config_name = ''
export(bool) var save_current = false

export(Array,Dictionary) var configs

export(int,0,10) var config_id = 0
export(bool) var load_config = false

var objs = []

func store_config():
	var c = {
		'config_name':config_name,
		'camera_speed':camera_speed,
		'cube_size_variation':cube_size_variation,
		'cube_rot_variation':cube_rot_variation,
		'tree_size_variation':tree_size_variation,
		'tree_rot_variation':tree_rot_variation,
		'cube_tree_ratio':cube_tree_ratio,
		'max_instances':max_instances,
		'linear_speed':linear_speed,
		'linear_decay':linear_decay,
		'radial_speed':radial_speed,
		'radial_decay':radial_decay,
		'start_min':start_min,
		'start_max':start_max
	}
	configs.append( c )

func restore_config():
	if config_id < 0 or config_id >= len( configs ):
		return
	var c = configs[config_id]
	config_name = c.config_name
	camera_speed = c.camera_speed
	cube_size_variation = c.cube_size_variation
	cube_rot_variation = c.cube_rot_variation
	tree_size_variation = c.tree_size_variation
	tree_rot_variation = c.tree_rot_variation
	cube_tree_ratio = c.cube_tree_ratio
	max_instances = c.max_instances
	linear_speed = c.linear_speed
	linear_decay = c.linear_decay
	radial_speed = c.radial_speed
	radial_decay = c.radial_decay
	start_min = c.start_min
	start_max = c.start_max

func _ready():
	config_id = 0
	load_config = true
	generate = true
	pass

func add_obj():
	
	var r = randf()
	
	var type = 'cube'
	if r < cube_tree_ratio:
		type = 'tree'
	
	var c = null
	if type == 'cube':
		c = $cube.duplicate()
	elif type == 'tree':
		c = $tree.duplicate()
	
	$generator.add_child( c )
	c.visible = true
	
	r = randf()
	c.translation.x = start_min.x * ( 1-r ) +  start_max.x * r
	r = randf()
	c.translation.y = start_min.y * ( 1-r ) +  start_max.y * r
	r = randf()
	c.translation.z = start_min.z * ( 1-r ) +  start_max.z * r
	
	if type == 'cube':
		c.scale *= Vector3(1,1,1) + cube_size_variation * randf()
		c.rotation_degrees = cube_rot_variation * Vector3(-0.5,0,-0.5) + cube_rot_variation * Vector3(0.5,1,0.5) * randf()
	elif type == 'tree':
		c.scale *= Vector3(1,1,1) + tree_size_variation * randf()
		c.rotation_degrees = tree_rot_variation * Vector3(-0.5,0,-0.5) + tree_rot_variation * Vector3(0.5,1,0.5) * randf()
	
	objs.append( {
		'type': type,
		'obj': c,
		'linear_vel': Vector3(),
		'radial_vel': Vector3()
	})

func trash_obj(i):
	$generator.remove_child( objs[i].obj )
	objs.remove(i)

func purge_until(i):
	while len(objs) > i:
		trash_obj(len(objs)-1)
	if i == 0:
		while $generator.get_child_count() > 0:
			$generator.remove_child( $generator.get_child(0) )

func _process(delta):
	
	$ui/debug.text = str(config_id) + " : " + configs[config_id].config_name
	
	if !generate:
		return
	
	if purge_all:
		purge_all = false
		purge_until(0)
	
	if len(objs) < max_instances:
		add_obj()
	else:
		purge_until(max_instances)
	
	if randomise_linear:
		randomise_linear = false
		for o in objs:
			o.linear_vel = linear_speed * -1 + linear_speed * 2 * randf()
	
	if randomise_radial:
		randomise_radial = false
		for o in objs:
			o.radial_vel = radial_speed * -1 + radial_speed * 2 * randf()
	
	if save_current:
		save_current = false
		store_config()
	
	if load_config:
		load_config = false
		restore_config()
	
	var to_trash = []
	var oi = 0
	
	for o in objs:
		
		o.obj.translation += camera_speed * delta
		o.obj.translation += o.linear_vel * delta
		o.obj.rotation += o.radial_vel * delta
		
		o.linear_vel *= (1-linear_decay)
		o.radial_vel *= (1-radial_decay)
		
		if o.obj.translation.x < end_min.x or o.obj.translation.y < end_min.y or o.obj.translation.z < end_min.z:
			to_trash.append( oi )
		if o.obj.translation.x > end_max.x or o.obj.translation.y > end_max.y or o.obj.translation.z > end_max.z:
			to_trash.append( oi )
		oi += 1
	for i in to_trash:
		trash_obj(i)

func _input(event):
	
	if event is InputEventKey:
		if event.is_pressed() and event.scancode == 32:
			generate = !generate
		elif event.is_pressed() and event.scancode == KEY_L:
			randomise_linear = true
		elif event.is_pressed() and event.scancode == KEY_R:
			randomise_radial = true
		elif event.is_pressed() and event.scancode == KEY_ENTER:
			config_id += 1
			config_id %= len(configs)
			load_config = true
